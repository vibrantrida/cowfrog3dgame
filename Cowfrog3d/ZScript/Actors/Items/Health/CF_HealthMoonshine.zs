class CF_HealthMoonshine : CustomInventory {
  Default {
    //$Category CowFrog3d/Items/Health
    //$Title Moonshine
    //$Sprite ALCPA0
    Inventory.PickupMessage "Got Moonshine";
    //Inventory.PickupSound "misc/p_pkup";
    +COUNTITEM
    +INVENTORY.ALWAYSPICKUP
    +INVENTORY.ISHEALTH
  }
  States {
    Spawn:
      ALCP A -1;
      Stop;
    Pickup:
      TNT1 A 0 A_GiveInventory("CF_PowerupDrunk");
      TNT1 A 0 HealThing(100, 0);
      Stop;
  }
}
