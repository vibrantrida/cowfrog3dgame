class CF_BasePlayer : PlayerPawn {
  Default {
    Speed 1;
    Health 100;
    Radius 16;
    Height 56;
    Mass 100;
    Scale 2;
    Player.DisplayName "CF_BasePlayer";
  }
  States {
    Spawn:
      TMPC A -1;
      Loop;
    See:
      TMPC EFGHIJ 4;
      Loop;
    Missile:
      TMPC A 4;
      TMPC B 2 Bright;
      TMPC CD 4;
      Goto Spawn;
    Melee:
      Goto Missile;
    Pain:
      Goto Spawn;
    Death:
      TMPC KLMN 4;
      TMPC O -1;
      Stop;
    XDeath:
      Goto Death;
    Lassoed:
      TMPC P -1;
      Loop;
  }
}
