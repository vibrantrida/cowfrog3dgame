// this is based on Belmondo's ZTracers and should be used instead of ZTracers' base class
class CF_BaseTracer : FastProjectile {
  const TRACERDURATION  = 1;
  const TRACERFANCY     = 1;
  const TRACERSCALE     = 4.0;
  const TRACERSTEP      = 0.01;

  String TRACERACTOR;

  double TRACERLENGTH;
  double TRACERFANCYSTEP;

  property Trail: TRACERACTOR, TRACERLENGTH, TRACERFANCYSTEP;

  double x1, y1, z1;
  double x2, y2, z2;

  static const color colours[] = {
    "9b 5b 13",
    "af 7b 1f",
    "c3 9b 2f",
    "d7 bb 43",
    "ff ff 73",
    "ff ff 73"
  };

  double lerp(double v0, double v1, double t) {
    return (1 - t) * v0 + t * v1;
  }

  override void BeginPlay() {
    x1 = pos.x;
    y1 = pos.y;
    z1 = pos.z;

    x2 = pos.x;
    y2 = pos.y;
    z2 = pos.z;
  }

  override void Tick() {
    if (isFrozen() || level.isFrozen()) return;

    x1 = pos.x;
    y1 = pos.y;
    z1 = pos.z;

    x2 = pos.x + vel.x / GetDefaultSpeed("CF_BaseTracer") * TRACERLENGTH;
    y2 = pos.y + vel.y / GetDefaultSpeed("CF_BaseTracer") * TRACERLENGTH;
    z2 = pos.z + vel.z / GetDefaultSpeed("CF_BaseTracer") * TRACERLENGTH;

    if (TRACERFANCY == 0) {
      for (double i = 0; i < 1; i += TRACERSTEP) {
        A_SpawnParticle (
          colours[clamp(i * colours.Size(), 0, colours.Size() - 1)],
          SPF_FULLBRIGHT,
          TRACERDURATION,
          TRACERSCALE * i,
          0,
          (-pos.x) + lerp(x1, x2, i),
          (-pos.y) + lerp(y1, y2, i),
          (-pos.z) + lerp(z1, z2, i),
          0, 0, 0,
          0, 0, 0,
          1.0
        );
      }
    } else {
      for (double i = 0; i < 1; i += TRACERFANCYSTEP) {
        A_SpawnItemEx (
          TRACERACTOR,
          (-pos.x) + lerp(x1, x2, i),
          (-pos.y) + lerp(y1, y2, i),
          (-pos.z) + lerp(z1, z2, i),
          0, 0, 0, 0,
          SXF_ABSOLUTEPOSITION
        );
      }
    }

    Super.Tick();
  }
}

extend class CF_BaseTracer {
  Default {
    Height 2;
    MissileHeight 8;
    Radius 2;
    Speed 80;
    Alpha 0.75;
    RenderStyle "Add";
    Scale 0.5;
    +NOEXTREMEDEATH;
    +BLOODSPLATTER;
    +SEEKERMISSILE;

    CF_BaseTracer.Trail "CF_BaseTrail", 32.0, 0.05;
  }
  States {
    Spawn:
      TNT1 A 1 Bright;
      Loop;
    Death:
      TNT1 A 0 {
        // play sound and spawn puff effect
      }
      Stop;
    XDeath:
      TNT1 A 0;
      Stop;
    Crash:
      TNT1 A 0 {
        // play sound and spawn puff effect
      }
      Stop;
  }
}
