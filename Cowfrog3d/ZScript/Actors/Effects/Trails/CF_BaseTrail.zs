class CF_BaseTrail : actor {
  Default {
    +FORCEXYBILLBOARD
    +NOINTERACTION
    +NOBLOCKMAP
    +NOCLIP
    +NOGRAVITY
    -SOLID
  }
  States {
    Spawn:
      TRL1 B 2 BRIGHT;
      Stop;
  }
}
