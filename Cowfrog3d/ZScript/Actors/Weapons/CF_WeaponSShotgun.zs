class CF_WeaponSShotgun : CF_BaseWeapon {
  Default {
    //$Category CowFrog3d/Weapons
    //$Title Super Shotgun
    //$Sprite RFLPA0
    Weapon.SlotNumber 1;
    Tag "$WT_SSHOTGUN";
  }
  States {
    Spawn:
      RFLP A -1;
      Loop;
    Select:
      TNT1 AAA 0 A_Raise;
      RFLR A 1 A_Raise;
      Loop;
    Deselect:
      TNT1 AAA 0 A_Lower;
      RFLR A 1 A_Lower;
      Loop;
    Ready:
      RFLR A 1 A_WeaponReady();
      Loop;
    Fire:
      TNT1 A 0 A_JumpIfInventory("CF_AmmoSShotgun", 1, 1);
      Goto Reload;
      RFLF AB 2;
      RFLF CD 1;
      RFLF E 2 {
        int drunk;
        drunk = 1;
        if (FindInventory("CF_PowerupDrunk")) {
          drunk = 4;
        }
        for (int i = 0; i < 7; i++) {
          A_FireProjectile("CF_ProjectileShotgun", frandom(-3.5, 3.5), 0, 0, 0, FPF_AIMATANGLE, frandom(-2.5, 2.5));
        }
        A_TakeInventory("CF_AmmoSShotgun", 1);
        A_Recoil(1*drunk);
        A_SetAngle(angle+frandom(-1*drunk,1*drunk));
        A_SetPitch(pitch-frandom(1.5*drunk,5.0*drunk));
        A_ZoomFactor(0.99,ZOOM_INSTANT);
      }
      RFLF F 2 {
        A_ZoomFactor(1);
        A_WeaponReady(WRF_NOFIRE | WRF_NOSWITCH | WRF_ALLOWRELOAD);
      }
      RFLF GH 2 A_WeaponReady(WRF_NOFIRE | WRF_NOSWITCH | WRF_ALLOWRELOAD);
      RFLL A 2 A_Refire();
      Goto Ready;
    Reload:
      RFLF A 4 A_WeaponOffset(0, 100, WOF_ADD);
      TNT1 A 1 A_GiveInventory("CF_AmmoSShotgun", 2);
      TNT1 A 15;
      RFLF A 4 A_WeaponOffset(0, -100, WOF_ADD);
      Goto Ready;
  }
}
