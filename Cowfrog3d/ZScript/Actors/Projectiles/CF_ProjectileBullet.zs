class CF_ProjectileBullet : CF_BaseTracer {
  Default {
    DamageFunction (randompick(8, 10, 12));
    CF_BaseTracer.Trail "CF_BaseTrail", 16.0, 0.5;
  }
}
