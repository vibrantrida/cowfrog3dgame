class CF_ProjectileLasso : Actor {
	default {
		Speed 8;
		Radius 10;
		Height 8;
		Damage 2;
		//DamageType "Ice";
		//RenderStyle "Add";
		+NOBLOCKMAP
		+NOGRAVITY
		+DROPOFF
		+MISSILE
		+NOTELEPORT
	}
	
	states {
		Spawn:
			LTST A 2 rope;
			loop;
		Death:
			LTST A 4 Bright;
			LTST A 2 Bright;
			LTST A 0;
			stop;
	}
	void rope ()
	{
		A_Log("Rope was created");
		//A_StartSound ("loremaster/active", CHAN_BODY);
		Spawn("CF_RopeProjectile", Pos, ALLOW_REPLACE);
		Spawn("CF_RopeProjectile", Vec3Offset(-Vel.x/2., -Vel.y/2., -Vel.z/2.), ALLOW_REPLACE);
		Spawn("CF_RopeProjectile", Vec3Offset(-Vel.x, -Vel.y, -Vel.z), ALLOW_REPLACE);
	}
}