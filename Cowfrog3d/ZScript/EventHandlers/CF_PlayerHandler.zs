class CF_PlayerHandler : EventHandler {
  int kills[32];
  int deaths[32];
  override void PlayerEntered(PlayerEvent e) {
    kills[e.PlayerNumber] = 0;
    deaths[e.PlayerNumber] = 0;
    Console.Printf("You're ID is %d", e.PlayerNumber);
  }

  override void PlayerRespawned(PlayerEvent e) {
    PlayerInfo player = players[e.PlayerNumber];
    deaths[e.PlayerNumber] += 1;
    player.mo.GiveInventory("CF_DeathCounter", 1);
  }

  override void WorldThingDied(WorldEvent e) {
    PlayerInfo killer;
    if (e.thing && e.thing.bIsMonster) {
      if (e.thing.target.player) {
        killer = players[e.Thing.target.player.mo.PlayerNumber()];
        kills[killer.mo.PlayerNumber()] += 1;
        killer.mo.GiveInventory("CF_KillCounter", 1);
      }
    }
  }
}
