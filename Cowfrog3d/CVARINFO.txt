//Vendor CVars

// Tilt++
// Strafe tilting
server bool sv_strafetilt = true;
server bool sv_strafetiltinvert = false;
server float sv_strafetiltspeed = 1.0;
server float sv_strafetiltangle = 0.5;

// Movement tilting
server bool sv_movetilt = false;
server float sv_movetiltspeed = 15.000;
server float sv_movetiltangle = 0.015;
server float sv_movetiltscalar = 0.2;

// Turn tilting
server bool sv_turntilt = true;
server float sv_turntiltscalar = 1.5;
server bool sv_turntiltinvert = false;

// Underwater tilting
server bool sv_underwatertilt = true;
server float sv_underwatertiltspeed = 0.8;
server float sv_underwatertiltangle = 0.2;
server float sv_underwatertiltscalar = 1.0;

// Death tilting
server bool sv_deathtilt = true;

// SpriteShadow
user int cl_spriteshadowdistance = 1500;
user int cl_maxspriteshadows = 200;
