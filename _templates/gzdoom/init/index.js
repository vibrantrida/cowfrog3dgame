const path = require('path')

const projectConfig = require(path.join(process.cwd(), 'project.config.json'))

module.exports = {
  params: () => {
    return {
      ...projectConfig
    }
  }
}
